package com.local.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.local.DSA.set.SimpleSet;

public abstract class SetTests {
    protected SimpleSet<Integer> set;
    protected SimpleSet<Integer> setB;
    
    @Before
    public void prepare() {
        set.add(1);
        set.add(2);
        set.add(3); 
        
        setB.add(0);
        setB.add(1);
        setB.add(2);
    }
    
    @Test
    public void testAdd() {
        assertEquals(false, set.add(1));
        assertEquals(true, set.add(4));
    }
    
    @Test
    public void testContains() {
        assertEquals(true, set.contains(1));
        assertEquals(false, set.contains(4));
    }
    
    @Test
    public void testDelete() {
        assertEquals(true, set.delete(1));
        assertEquals(false, set.delete(4));
    }
    
    @Test
    public void testIntersection() {
        SimpleSet<Integer> resultSet = set.intersection(setB);
        System.out.println("intersection:");
        resultSet.forEach(System.out::println);
        
        resultSet.forEach((element) -> {
            boolean result = set.contains(element) && setB.contains(element);
            assertEquals(result, resultSet.contains(element));
        });
        set.forEach((element) -> {
            boolean result = setB.contains(element);
            assertEquals(result, resultSet.contains(element));
        });
        setB.forEach((element) -> {
            boolean result = set.contains(element);
            assertEquals(result, resultSet.contains(element));
        });
    }
    
    @Test
    public void testUnion() {
        SimpleSet<Integer> resultSet = set.union(setB);
        System.out.println("union:");
        resultSet.forEach(System.out::println);

        resultSet.forEach((element) -> {
            boolean result = set.contains(element) || setB.contains(element);
            assertEquals(result, resultSet.contains(element));
        });
    }
    
}
