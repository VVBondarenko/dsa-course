package com.local.test;

import com.local.DSA.sort.impl.InsertSort;

public class InsertSortTests extends SortTests {
    @Override
    public void prepare() {
        super.prepare();
        sort = new InsertSort<Integer>(data);
    }
}
