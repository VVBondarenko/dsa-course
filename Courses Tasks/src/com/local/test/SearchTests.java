package com.local.test;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.local.DSA.search.ElementNotFoundException;
import com.local.DSA.search.Search;

public abstract class SearchTests {
    protected Search<Integer> search;
    private List<Integer> listToSearchIn = new LinkedList<>();
    private List<Integer> searchAndFind = new LinkedList<>();
    private List<Integer> searchAndNotFind = new LinkedList<>();    
    
    @Before
    public void prepare() {
        listToSearchIn.add(0);
        listToSearchIn.add(2);
        listToSearchIn.add(4);
        listToSearchIn.add(6);
        listToSearchIn.add(6);
        listToSearchIn.add(8);
        listToSearchIn.add(9);
        listToSearchIn.add(100);
        listToSearchIn.add(105);
        listToSearchIn.add(600);
        listToSearchIn.add(990);
        
        searchAndFind.add(0);
        searchAndFind.add(2);
        searchAndFind.add(6);
        searchAndFind.add(105);
        searchAndFind.add(990);
        
        searchAndNotFind.add(999);
        searchAndNotFind.add(78778);
        searchAndNotFind.add(1);
    }
    
    @Test
    public void testSuccessfulSearches() {
        for (Integer element : searchAndFind) {
            Integer actualFoundElement = search.findElementInList(element, listToSearchIn);
            assertEquals(element, actualFoundElement);
            assertEquals(true, actualResultContainedInList(actualFoundElement));
        }
    }
    
    @Test
    public void testFailingSearches() {
        for (Integer element : searchAndNotFind) {
            try {
                search.findElementInList(element, listToSearchIn);
            } catch (Exception e) {
                assertEquals(true, e instanceof RuntimeException);
                assertEquals(true, e instanceof ElementNotFoundException);
            }
        }
    }
    
    private boolean actualResultContainedInList(Integer actualElement) {
        for (Integer element : listToSearchIn)
            if (element == actualElement)
                return true;
        return false;
    }
    
}
