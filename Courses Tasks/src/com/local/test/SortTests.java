package com.local.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.local.DSA.sort.Sort;

public abstract class SortTests {
    protected Sort<Integer> sort;
    protected List<Integer> data = new LinkedList<>();

    @Before
    public void prepare() {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            data.add(random.nextInt(64));
        }
    }
   
    @Test
    public void testAscSort() {
        List<Integer> sorted = sort.sortAsc();
        assertEquals(data.size(), sorted.size());
        Order.asc.check(sorted);
    }
    
    @Test
    public void testDescSort() {
        List<Integer> sorted = sort.sortDesc();
        assertEquals(data.size(), sorted.size());
        Order.desc.check(sorted);
    }
}

enum Order {
    asc, desc;
    
    public void check(List<Integer> sortedList) {
        Iterator<Integer> iterator = sortedList.iterator();
        Integer previous = iterator.next();
        
        int ignoringComparedValue = getIgnoredCompareValue();
        
        while(iterator.hasNext()) {
            Integer value = iterator.next();
            assertNotSame(ignoringComparedValue, value.compareTo(previous));
            previous = value;
        }
    }

    private int getIgnoredCompareValue() {
        int ignoringComparedValue = 0;
        if (this == Order.asc)
            ignoringComparedValue = -1;
        if (this == Order.desc) 
            ignoringComparedValue = 1;
        return ignoringComparedValue;
    }
}
