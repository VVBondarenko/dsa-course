package com.local.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.local.DSA.lists.IndexOutOfRangeException;
import com.local.DSA.lists.SimpleList;

public abstract class ListTests {
    protected SimpleList<Integer> list;

    @Before
    public void prepare() {
        assertNotNull(list);

        list.add(1);
        list.add(2);
        list.add(3);
    }

    @Test
    public void testGetters() {
        assertEquals(3, list.size());
        assertEquals(false, list.isEmpty());
        assertEquals(1, list.get(0).intValue());
        assertEquals(2, list.get(1).intValue());
        assertEquals(3, list.get(2).intValue());
    }

    @Test(expected = IndexOutOfRangeException.class)
    public void testNegativeIndex() {
        try {
            list.get(-1);
        } catch (Exception e) {
            assertEquals("Index out of range", e.getMessage());
            throw e;
        }
    }

    @Test(expected = IndexOutOfRangeException.class)
    public void testOutOfRange() {
        try {
            list.get(100);
        } catch (Exception e) {
            assertEquals("Index out of range", e.getMessage());
            throw e;
        }
    }

    @Test
    public void testContains() {
        assertEquals(true, list.containts(1));
        assertEquals(false, list.containts(1488));
    }

    @Test
    public void testDelete() {
        assertEquals("123", formString(list));
        assertEquals(false, list.delete(1488));
        assertEquals("123", formString(list));

        assertEquals(true, list.delete(2));
        assertEquals("13", formString(list));

        assertEquals(true, list.delete(3));
        assertEquals("1", formString(list));

        assertEquals(true, list.delete(1));
        assertEquals(0, list.size());
        assertEquals(true, list.isEmpty());
        assertEquals(false, list.delete(0));
    }

    @Test
    public void testAdd() {
        list.add(50);
        assertEquals("12350", formString(list));
        assertEquals(4, list.size());
    }

    private String formString(SimpleList<?> list) {
        StringBuffer buffer = new StringBuffer();
        list.forEach(buffer::append);
        return buffer.toString();
    }
}
