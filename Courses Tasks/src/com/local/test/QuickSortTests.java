package com.local.test;

import com.local.DSA.sort.impl.QuickSort;

public class QuickSortTests extends SortTests {
    @Override
    public void prepare() {
        super.prepare();
        sort = new QuickSort<Integer>(data);
    }
}
