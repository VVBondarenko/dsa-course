package com.local.test;

import java.util.Comparator;

import com.local.DSA.search.impl.BinarySearch;

public class BinarySearchTests extends SearchTests {
    @Override
    public void prepare() {
        this.search = new BinarySearch<Integer>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2.compareTo(o1);
            }
        });
        super.prepare();
    }
}
