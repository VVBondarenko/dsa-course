package com.local.test;

import com.local.DSA.sort.impl.MergeSort;

public class MergeSortTests extends SortTests {
    @Override
    public void prepare() {
        super.prepare();
        sort = new MergeSort<Integer>(data);
    }
}
