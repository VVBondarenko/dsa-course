package com.local.DSA.search.impl;

import java.util.Comparator;
import java.util.List;

import com.local.DSA.search.ElementNotFoundException;
import com.local.DSA.search.Search;

public class ProbabilitySearch<T> implements Search<T> {
    private Comparator<T> comparator;

    public ProbabilitySearch(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    @Override
    public T findElementInList(T element, List<T> list) {
        for (int i = 0; i < list.size(); i++) {
            T listElement = list.get(i);
            if (comparator.compare(element, listElement) != 0)
                continue;
            
            if (i > 0) {
                list.remove(i);
                list.add(i-1, listElement);
            }
            
            return listElement;
        }
        throw new ElementNotFoundException();
    }
}
