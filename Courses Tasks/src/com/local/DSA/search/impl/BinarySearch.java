package com.local.DSA.search.impl;

import java.util.Comparator;
import java.util.List;

import com.local.DSA.search.ElementNotFoundException;
import com.local.DSA.search.Search;
import com.local.DSA.tree.BSTImpl;

public class BinarySearch<T> implements Search<T> {
    private Comparator<T> comparator;

    public BinarySearch(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    @Override
    public T findElementInList(T element, List<T> list) {
        if (list.isEmpty())
            throw new ElementNotFoundException();

        int size = list.size();
        int middle = size / 2;
        T value = list.get(middle);
        int compareResult = comparator.compare(element, value);
        
        if(compareResult == 0)
            return value;
        
        if (compareResult > 0) {
            List<T> leftSubList = list.subList(0, middle);
            return findElementInList(element, leftSubList);
        }

        if (compareResult < 0) {
            List<T> rightSubList = list.subList(middle + 1, size);
            return findElementInList(element, rightSubList);
        }

        throw new ElementNotFoundException();
    }
}
