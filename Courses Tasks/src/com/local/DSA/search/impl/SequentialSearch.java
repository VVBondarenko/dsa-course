package com.local.DSA.search.impl;

import java.util.Comparator;
import java.util.List;

import com.local.DSA.search.ElementNotFoundException;
import com.local.DSA.search.Search;

public class SequentialSearch<T> implements Search<T> {
    private Comparator<T> comparator;

    public SequentialSearch(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    @Override
    public T findElementInList(T element, List<T> list) {
        for (T listElement : list)
            if (comparator.compare(listElement, element) == 0)
                return listElement;
        throw new ElementNotFoundException();
    }
}
