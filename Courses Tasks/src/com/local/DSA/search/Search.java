package com.local.DSA.search;

import java.util.Comparator;
import java.util.List;

public interface Search<T> {
    T findElementInList(T element, List<T> list);
}
