package com.local.DSA.tree;

public class AVLImpl<T extends Comparable<T>> extends BSTImpl<T> {
    private void balance() {
        BST<T> originalLeft = left;
        BST<T> originalRight = right;
        int heightDifference = originalLeft.height() - originalRight.height();
        if (heightDifference > 1) {
            if (Math.abs(originalLeft.getLeft().height() - originalLeft.getRight().height()) > 0)
                originalLeft.promote();
            else {
                originalLeft.promote();
                originalLeft.promote();
            }
        } else if (heightDifference < -1) {
            if (Math.abs(originalRight.getLeft().height() - originalRight.getRight().height()) > 0)
                originalRight.promote();
            else {
                originalRight.promote();
                originalRight.promote();
            }
        }
    }
    
    @Override
    public boolean insert(T element) {
        boolean result = super.insert(element);
        if (result)
            balance();
        return result;
    }
}
