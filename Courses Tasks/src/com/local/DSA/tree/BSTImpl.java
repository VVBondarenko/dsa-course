package com.local.DSA.tree;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import com.sun.istack.internal.NotNull;

public class BSTImpl<T extends Comparable<T>> implements BST<T> {
    protected T value;

    protected BST<T> parent;
    protected BST<T> left;
    protected BST<T> right;

    public BSTImpl() {
        value = null;
        left = null;
        right = null;
        parent = null;
    }

    public BSTImpl(List<T> list) {
        int size = list.size();
        int middle = size / 2;
        value = list.get(middle);

        if (middle > 0) {
            List<T> leftSubList = list.subList(0, middle);
            left = new BSTImpl<>(leftSubList, this);
        }

        if (middle + 1 < size) {
            List<T> rightSubList = list.subList(middle + 1, size);
            right = new BSTImpl<>(rightSubList, this);
        }
    }

    private BSTImpl(List<T> list, BST<T> parent) {
        this(list);
        this.parent = parent;
    }

    public BSTImpl(T value) {
        this.value = value;

        left = null;
        right = null;
        parent = null;
    }

    public BSTImpl(T value, BST<T> parent) {
        this(value);
        this.parent = parent;
    }

    @Override
    public BST<T> getParent() {
        return parent;
    }

    @Override
    public void setParent(BST<T> parent) {
        this.parent = parent;
    }

    @Override
    public BST<T> getRight() {
        return right;
    }

    @Override
    public BST<T> getLeft() {
        return left;
    }

    @Override
    public void setRight(BST<T> right) {
        this.right = right;
    }

    @Override
    public void setLeft(BST<T> left) {
        this.left = left;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public boolean insert(T element) {
        if (value == null) {
            value = element;
            return true;
        }

        int compareResult = value.compareTo(element);
        if (compareResult == 0)
            return false;

        if (compareResult < 0) {
            if (left == null) {
                left = new BSTImpl<T>(element, this);
                return true;
            }
            return left.insert(element);
        } else {
            if (right == null) {
                right = new BSTImpl<T>(element, this);
                return true;
            }
            return right.insert(element);
        }
    }

    @Override
    public boolean contains(T element) {
        BST<T> node = findNode(element);
        return node != null;
    }

    @Override
    public BST<T> findParentOf(T element) {
        BST<T> node = findNode(element);
        if (node == null)
            return null;
        return node.getParent();
    }

    @Override
    public BST<T> findNode(T element) {
        if (value == null)
            throw new TreeConstructionException("Empty BST");

        int compareResult = value.compareTo(element);
        if (compareResult == 0)
            return this;
        if (compareResult < 0) {
            if (left == null)
                return null;
            return left.findNode(element);
        } else {
            if (right == null)
                return null;
            return right.findNode(element);
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean delete(T element) {
        BST<T> nodeToRemove = findNode(element);
        if (nodeToRemove == null || !(nodeToRemove instanceof BSTImpl))
            return false;
        return ((BSTImpl) nodeToRemove).delete();
    }

    private boolean delete() {
        if (parent == null) {
            if (left != null)
                left.promote();
            else if (right != null)
                right.promote();
            else
                return false;
        }

        BST<T> toPromote = left;
        if (toPromote == null)
            toPromote = right;

        if (toPromote != null)
            toPromote.setParent(parent);

        parent.replaceChildWith(this, toPromote);

        if (left != null && right != null)
            right.forEach(left::insert);

        return true;
    }

    @Override
    public void forEach(Consumer<T> lambda) {
        if (left != null)
            left.forEach(lambda);
        lambda.accept(value);
        if (right != null)
            right.forEach(lambda);
    }

    @Override
    public void forEachNode(Consumer<BST<T>> lambda) {
        if (left != null)
            left.forEachNode(lambda);
        lambda.accept(this);
        if (right != null)
            right.forEachNode(lambda);
    }

    @Override
    public int height() {
        final List<Integer> heights = new LinkedList<>();
        forEachNode(node -> {
            if (node.isLeaf())
                heights.add(node.depth());
        });

        return Collections.max(heights);
    }

    @Override
    public int depth() {
        int depth = 0;
        for (BST<T> node = parent; node != null; node = node.getParent())
            depth++;
        return depth;
    }

    @Override
    public void promote() {
        if (parent == null)
            throw new TreeConstructionException("Promoting root");

        if (parent.getLeft() == this) {
            rotateRight();
        } else {
            rotateLeft();
        }
    }

    private void rotateRight() {
        BST<T> movingParent = parent;
        parent = movingParent.getParent();
        if (parent != null)
            parent.replaceChildWith(movingParent, this);

        BST<T> movingRightBranch = right;
        right = movingParent;
        movingParent.setParent(this);
        movingParent.setLeft(movingRightBranch);
        if (movingRightBranch != null)
            movingRightBranch.setParent(movingParent);
    }

    private void rotateLeft() {
        BST<T> movingParent = parent;
        parent = movingParent.getParent();
        if (parent != null)
            parent.replaceChildWith(movingParent, this);

        BST<T> movingLeftBranch = left;
        left = movingParent;
        movingParent.setParent(this);
        movingParent.setRight(movingLeftBranch);
        movingLeftBranch.setParent(movingParent);
    }

    @Override
    public void replaceChildWith(@NotNull BST<T> child, BST<T> replacement) {
        if (left == child) {
            left = replacement;
            return;
        }
        if (right == child) {
            right = replacement;
            return;
        }
        throw new TreeConstructionException("Unable to replace child");
    }

    @Override
    public String toString() {
        String valueString = toStringOrNull(this);
        String valueClass = (value != null) ? value.getClass().getSimpleName() : "?";
        String leftValueString = toStringOrNull(left);
        String rightValueString = toStringOrNull(right);
        return String.format("BST<%s>: %s {%s, %s}", valueClass, valueString, leftValueString, rightValueString);
    }

    private String toStringOrNull(BST<T> value) {
        if (value == null)
            return "null";

        T nodeValue = value.getValue();
        if (nodeValue == null)
            return "null";

        return nodeValue.toString();
    }

    @Override
    public boolean isLeaf() {
        return right == null && left == null;
    }
}
