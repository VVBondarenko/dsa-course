package com.local.DSA.tree;

public class TreeConstructionException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public TreeConstructionException(String message) {
        super(message);
    }
}
