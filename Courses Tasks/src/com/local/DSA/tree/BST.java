package com.local.DSA.tree;

import java.util.function.Consumer;

public interface BST<T extends Comparable<T>> {
    BST<T> getLeft();
    BST<T> getRight();
    BST<T> getParent();
    T getValue();
    void setParent(BST<T> newParent);
    void setLeft(BST<T> newLeft);
    void setRight(BST<T> newRight);
    
    boolean insert(T element);
    boolean contains(T element);
    boolean delete(T element);
    boolean isLeaf();
    
    BST<T> findNode(T element);
    BST<T> findParentOf(T element);
    void forEach(Consumer<T> lambda);
    void forEachNode (Consumer<BST<T>> lambda);
    int height();
    int depth();
    
    void promote();
    void replaceChildWith(BST<T> child, BST<T> replacement);
}
