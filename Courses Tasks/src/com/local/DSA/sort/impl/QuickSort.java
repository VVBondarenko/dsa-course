package com.local.DSA.sort.impl;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.local.DSA.sort.Sort;

public class QuickSort<T extends Comparable<T>> implements Sort<T> {
    private List<T> sorting = new LinkedList<>();
    private Comparator<T> comparator = null;
    private Random random = new Random();

    public QuickSort(List<T> toSort) {
        sorting.addAll(toSort);
    }

    @Override
    public List<T> sortAsc() {
        comparator = new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o2.compareTo(o1);
            }
        };
        return sort(sorting);
    }

    @Override
    public List<T> sortDesc() {
        comparator = new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        };
        return sort(sorting);
    }

    protected List<T> sort(List<T> sortingPart) {
        if (sortingPart.size() <= 1)
            return sortingPart;
        
        int pivotIndex = random.nextInt(sortingPart.size());
        T pivot = sortingPart.get(pivotIndex);
        List<T> left = new LinkedList<>();
        List<T> equals = new LinkedList<>();
        List<T> right = new LinkedList<>();
        for (T element : sortingPart) {
            int comparedToPivot = comparator.compare(pivot, element);
            if (comparedToPivot < 0)
                left.add(element);
            if (comparedToPivot == 0)
                equals.add(element);
            if (comparedToPivot > 0)
                right.add(element);
        }

        left = sort(left);
        left.addAll(equals);
        right = sort(right);
        left.addAll(right);
        return left;
    }

}
