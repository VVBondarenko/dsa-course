package com.local.DSA.sort.impl;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.local.DSA.sort.Sort;

public class InsertSort<T extends Comparable<T>> implements Sort<T> {
    private List<T> sorting = new LinkedList<>();
    private Comparator<T> comparator = null;

    public InsertSort(List<T> toSort) {
        sorting.addAll(toSort);
    }

    @Override
    public List<T> sortAsc() {
        comparator = new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o2.compareTo(o1);
            }
        };
        return sort();
    }

    @Override
    public List<T> sortDesc() {
        comparator = new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        };
        return sort();
    }

    protected List<T> sort() {
        if (sorting.size() <= 1)
            return sorting;
        for (int i = 1; i < sorting.size(); i++) {
            T hold = sorting.get(i);
            for (int j = 0; j < i; j++) {
                T comparing = sorting.get(j);

                if (comparator.compare(comparing, hold) < 0) {
                    sorting.remove(i);
                    sorting.add(j, hold);
                    break;
                }
            }
        }
        return sorting;
    }
}




















