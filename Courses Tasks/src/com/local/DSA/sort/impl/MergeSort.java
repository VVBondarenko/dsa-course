package com.local.DSA.sort.impl;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.local.DSA.sort.Sort;

public class MergeSort<T extends Comparable<T>> implements Sort<T> {
    private List<T> sorting = new LinkedList<>();
    private Comparator<T> comparator = null;

    public MergeSort(List<T> toSort) {
        sorting.addAll(toSort);
    }

    @Override
    public List<T> sortAsc() {
        comparator = new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o2.compareTo(o1);
            }
        };
        return sort(sorting);
    }

    @Override
    public List<T> sortDesc() {
        comparator = new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.compareTo(o2);
            }
        };
        return sort(sorting);
    }

    protected List<T> sort(List<T> part) {
        if (part.size() <= 1)
            return part;
        int size = part.size();
        int middle = size / 2;

        List<T> leftPart = null;
        if (middle > 0) {
            leftPart = part.subList(0, middle);
            leftPart = sort(leftPart);
        }

        List<T> rightPart = null;
        if (middle < size) {
            rightPart = part.subList(middle, size);
            rightPart = sort(rightPart);
        }

        if (leftPart == null || rightPart == null)
            return part;

        return merge(leftPart, rightPart);
    }

    protected List<T> merge(List<T> left, List<T> right) {
        if (left.isEmpty())
            return right;
        if (right.isEmpty())
            return left;

        Iterator<T> leftIterator = left.iterator();
        T leftValue = leftIterator.next();

        Iterator<T> rightIterator = right.iterator();
        T rightValue = rightIterator.next();

        List<T> result = new LinkedList<T>();
        while (true) {
            if (comparator.compare(leftValue, rightValue) > 0) {
                result.add(leftValue);
                if (leftIterator.hasNext()) {
                    leftValue = leftIterator.next();
                    continue;
                } else {
                    result.add(rightValue);
                    addEverythingLeftTo(result, rightIterator);
                    break;
                }
            } else {
                result.add(rightValue);
                if (rightIterator.hasNext()) {
                    rightValue = rightIterator.next();
                    continue;
                } else {
                    result.add(leftValue);
                    addEverythingLeftTo(result, leftIterator);
                    break;
                }
            }
        }
        return result;
    }

    private void addEverythingLeftTo(List<T> result, Iterator<T> iterator) {
        while (iterator.hasNext())
            result.add(iterator.next());
    }

}
