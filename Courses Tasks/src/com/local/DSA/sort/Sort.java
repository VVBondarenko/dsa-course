package com.local.DSA.sort;

import java.util.List;

public interface Sort<T extends Comparable<T>> {
    List<T> sortAsc();

    List<T> sortDesc();
}
