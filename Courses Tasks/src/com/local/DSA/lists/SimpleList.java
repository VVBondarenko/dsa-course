package com.local.DSA.lists;

import java.util.function.Consumer;

public interface SimpleList<T> {    
    void add(T element);
    boolean containts(T element);
    boolean delete(T element);
    T get(int i);
    int size();
    void forEach(Consumer<T> lambda);
    boolean isEmpty();
}
