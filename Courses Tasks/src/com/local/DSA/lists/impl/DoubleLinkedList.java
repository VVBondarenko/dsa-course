package com.local.DSA.lists.impl;

import java.util.function.Consumer;

import com.local.DSA.lists.IndexOutOfRangeException;
import com.local.DSA.lists.SimpleList;

public class DoubleLinkedList<T> implements SimpleList<T> {
    private Node first;
    private Node last;

    private int size = 0;

    @Override
    public void add(T element) {
        if (size == 0) {
            first = new Node(element);
            last = first;
            size++;
            return;
        }

        last = last.createNext(element);
        size++;
    }

    @Override
    public boolean containts(T element) {
        Node node = find(element);
        return node != null;
    }

    @Override
    public boolean delete(T element) {
        Node node = find(element);
        if (node == null)
            return false;
        if (node == first)
            first = node.getNext();
        if (node == last)
            last = node.getPrevious();
        node.delete();
        size--;
        return true;
    }

    private Node find(T element) {
        for (Node current = first; current != null; current = current.getNext()) {
            if (current.getElement().equals(element))
                return current;
        }
        return null;
    }

    @Override
    public T get(int i) {
        if (i < 0 || i >= size)
            throw new IndexOutOfRangeException();
        int j = 0;
        for (Node current = first; current != null; current = current.getNext()) {
            if (j == i)
                return current.element;
            j++;
        }
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void forEach(Consumer<T> lambda) {
        for (Node current = first; current != null; current = current.getNext()) {
            lambda.accept(current.element);
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    private class Node {
        private T element;
        private Node next = null;
        private Node previous = null;

        public Node(T element) {
            this.element = element;
        }

        public Node(T element, Node previous) {
            this.element = element;
            this.previous = previous;
        }

        public Node createNext(T element) {
            Node nextOne = new Node(element, this);
            this.next = nextOne;
            return nextOne;
        }

        public T getElement() {
            return element;
        }

        public Node getNext() {
            return next;
        }

        public Node getPrevious() {
            return previous;
        }

        public void delete() {
            if (previous != null)
                previous.next = this.next;
            if (next != null)
                next.previous = this.previous;

            this.next = null;
            this.previous = null;
        }
    }
}
