package com.local.DSA.lists.impl;

import java.util.Arrays;
import java.util.function.Consumer;

import com.local.DSA.lists.IndexOutOfRangeException;
import com.local.DSA.lists.SimpleList;

public class SimpleArrayList<T> implements SimpleList<T> {
    private T[] storage = null;
    private int size = 0;
    
    @SuppressWarnings("unchecked")
    @Override
    public void add(T element) {
        if (storage == null) 
            storage = (T[]) new Object[1];
        
        if (size >= storage.length)
            storage = Arrays.copyOf(storage, storage.length * 2);
        
        storage[size] = element;
        size++;
    }

    @Override
    public boolean containts(T element) {
        for (T t : storage)
            if (element.equals(t))
                return true;
        
        return false;
    }

    @Override
    public boolean delete(T element) {
        int foundOnPosition = -1;
        for (int i = 0; i < size; i++) {
            if (storage[i].equals(element)) {
                foundOnPosition = i;
                break;
            }
        }
        if (foundOnPosition < 0)
            return false;
        

        for (int i = foundOnPosition + 1; i < size; i++) {
            storage[i-1] = storage[i];
        }
        size--;
        return true;
    }

    @Override
    public T get(int i) {
        if (i < 0 || i >= size)
            throw new IndexOutOfRangeException();
        return storage[i];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void forEach(Consumer<T> lambda) {
        for (int i = 0; i < size; i++) {
            lambda.accept(storage[i]);
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
}
