package com.local.DSA.lists.impl;

import java.util.function.Consumer;

import com.local.DSA.lists.IndexOutOfRangeException;
import com.local.DSA.lists.SimpleList;

public class SimpleLinkedList<T> implements SimpleList<T> {
    private Node first = null;
    private Node last = null;
    private int size = 0;
    
    @Override
    public void add(T element) {
        Node node = new Node (element);
        
        if (size == 0) {
            first = node;
            last = node;
        } else
            last.setNext(node);
        
        last = node;
        size++;
    }

    @Override
    public boolean containts(T element) {
        for (Node current = first; current.hasNext(); current = current.next()) {
            T currentElement = current.getElement();
            if (element.equals(currentElement))
                return true;
        }
        return false;
    }

    @Override
    public boolean delete(T element) {
        if (isEmpty()) return false;
        
        Node previous = null;
        for (Node current = first; current != null; current = current.next()) {
            T currentElement = current.getElement();
            if (element.equals(currentElement)) {
                if (current == first) {
                    first = current.next();
                } else if (current == last) {
                    previous.setNext(null);
                    last = previous;
                } else {
                    previous.setNext(current.next());
                }
                size--;
                return true;
            }
            previous = current;
        }
        return false;
    }

    @Override
    public T get(int i) {
        if (i < 0 || i >= size)
            throw new IndexOutOfRangeException();
        int j = 0;
        for (Node current = first; current != null; current = current.next()) {
            if (j == i)
                return current.element;
            j++;
        }
        return null;
    }

    @Override
    public void forEach(Consumer<T> lambda) {
        for (Node current = first; current != null; current = current.next()) {
            lambda.accept(current.element);
        }
    }
    
    @Override
    public int size() {
        return size;
    }
    
    @Override
    public boolean isEmpty() {
        return first == null;
    }
    
    private class Node {
        private T element;
        private Node next = null;
        
        public Node(T element) {
            this.element = element;
        }

        public Node next() {
            return next;
        }
        
        public boolean hasNext() {
            return next != null;
        }

        public void setNext(Node nextNode) {
            this.next = nextNode;
        }
        
        public T getElement() {
            return element;
        }
    }
}
