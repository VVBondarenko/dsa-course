package com.local.DSA.lists;

public class IndexOutOfRangeException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Index out of range";
    }
}
