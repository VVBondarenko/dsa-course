package com.local.DSA.set.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.local.DSA.set.SimpleSet;

public class SimpleListSet<T> implements SimpleSet<T> {
    protected List<T> content = new ArrayList<>();

    @Override
    public boolean add(T element) {
        if (contains(element))
            return false;

        content.add(element);
        return true;
    }

    @Override
    public boolean contains(T element) {
        return content.contains(element);
    }

    @Override
    public boolean delete(T element) {
        return content.remove(element);
    }

    @Override
    public SimpleSet<T> intersection(SimpleSet<T> set) {
        SimpleSet<T> result = new SimpleListSet<T>();

        content.stream()
            .filter(set::contains)
            .forEach(result::add);
        return result;
    }

    @Override
    public SimpleSet<T> union(SimpleSet<T> set) {
        SimpleSet<T> result = copy();
        set.forEach(result::add);
        return result;
    }

    @SuppressWarnings("unchecked")
    private SimpleSet<T> copy() {
        SimpleListSet<T> copy = new SimpleListSet<T>();
        copy.content = (ArrayList<T>) ((ArrayList<T>) content).clone();
        return copy;
    }

    @Override
    public void forEach(Consumer<T> lambda) {
        content.forEach(lambda);
    }
}
