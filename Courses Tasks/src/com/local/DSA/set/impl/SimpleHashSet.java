package com.local.DSA.set.impl;

public class SimpleHashSet<T> extends SimpleListSet<T> {
    @Override
    public boolean contains(T element) {
        int hashCode = element.hashCode();
        for (T el : content) {
            if (el.hashCode() == hashCode && element.equals(el))
                return true;
        }
        return false;
    }
}
