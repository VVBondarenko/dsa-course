package com.local.DSA.set;

import java.util.function.Consumer;

public interface SimpleSet<T> {
    boolean add(T element);
    boolean contains(T element);
    boolean delete(T element);
    
    SimpleSet<T> intersection(SimpleSet<T> set);
    SimpleSet<T> union(SimpleSet<T> set);
    
    void forEach(Consumer<T> lambda);
}
