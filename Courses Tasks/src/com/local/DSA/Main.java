package com.local.DSA;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.local.DSA.tree.BST;
import com.local.DSA.tree.BSTImpl;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);

        BST<Integer> tree = new BSTImpl<>(list);
//        BST<Integer> oneNode = tree.getLeft().getLeft();
//        oneNode.promote();
//        oneNode.promote();
//        tree.forEachNode(node -> {
//            StringBuilder builder = new StringBuilder();
//            builder.append(node.getValue());
//            for (BST<Integer> parent = node.getParent(); parent != null; parent = parent.getParent()) {
//                builder.append(";").append(parent.getValue());
//            }
//            
//            builder.append("\t").append(node.height());
//            System.out.println(builder.toString());
//        });
        System.out.println(serialize(tree));
        
    }
    
    public static String serialize(Object obj) {
        Gson gson = new GsonBuilder().serializeNulls()
                .setPrettyPrinting()
                .create();

        return gson.toJson(obj);
    }

}
